from threading import *
import requests


class PyJax(Thread,  object):

    def __init__(self, thread_name="", callback = None):

        Thread.__init__(self)

        self.thread_name = thread_name

        self.url = None
        self.method = None

        self.authorization = False

        self.parmeters = None
        self.data = None
        self.json = None
        self.files = None

        self.headers = None
        self.verify = True
        self.res = None

        if callback:

            if not callable(callback):
                raise TypeError("Callback should be a `function` or `False`")

        self.call_back = callback

    def run(self):

        if self.method.lower() == "get":

            self.res = requests.get(self.url, data=self.data, params=self.parmeters, json=self.json,
                                    headers=self.headers, verify=self.verify, auth=self.authorization)

            if self.call_back:

                self.call_back(self.res)

        elif self.method.lower() == "post":

            self.res = requests.post(self.url, data=self.data, params=self.parmeters, json=self.json,
                                    headers=self.headers, verify=self.verify, auth=self.authorization)

            if self.call_back:

                self.call_back(self.res)

        else:
            raise ValueError("Invalid method. only `GET` and `POST` supported!!!")

    def execute(self):
        self.start()
